package com.mz.press.services;


import com.mz.press.exceptions.ItemNotFoundException;
import com.mz.press.models.Article;
import com.mz.press.repositories.ArticleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class ArticleService {

    private final ArticleRepository repository;

    public Article addArticle(Article article) {
        article.setCreatedAt(LocalDateTime.now());
        return repository.save(article);
    }

    public Article updateArticle(Long id, Article formArticle) {
        var optionalArticle = findArticle(id);
        if (optionalArticle.isPresent()) {
            var dbArticle = optionalArticle.get();
            dbArticle.setTitle(formArticle.getTitle());
            dbArticle.setContent(formArticle.getContent());
            dbArticle.setAuthor(formArticle.getAuthor());
            dbArticle.setMagazine(formArticle.getMagazine());
            dbArticle.setPublished(formArticle.getPublished());
            dbArticle.setUpdatedAt(LocalDateTime.now());
            return repository.save(dbArticle); // brakowało return w przeciwnym wypadku zawsze wykonywał się throw
        }
        throw new ItemNotFoundException("Artykuł nie został odnaleziony");
    }

    public void removeArticle(Long id) {
        repository.deleteById(id);
    }

    public Optional<Article> findArticle(Long id) {
        return repository.findById(id);
    }

    public List<Article> getArticleByParams(String title, String content) {
        if (title == null && content == null) {
            return repository.findAll();
        }
        return repository.findByParams(title, content);
    }

}
