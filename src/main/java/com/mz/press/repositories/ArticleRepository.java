package com.mz.press.repositories;


import com.mz.press.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Query("""
            select a from Article a where
             a.title like %:title% or
             a.content like %:content%
            """)
    List<Article> findByParams(
            @Param("title") String title,
            @Param("content") String content
    );
}
