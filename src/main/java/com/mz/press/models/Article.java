package com.mz.press.models;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Builder
@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Tytuł nie może być pusty")
    @Size(min = 5, max = 70, message = "Tytuł wymaga od 5-70 znaków")
    @Column(length = 70)
    private String title;

    @NotBlank(message = "Treść nie może być pusta")
    @Size(min = 5, max = 1023, message = "Treść wymaga od 5-1023 znaków")
    @Column(length = 1023)
    private String content;

    @NotBlank(message = "Nazwa czasopisma nie może być pusta")
    @Size(min = 3, max = 20, message = "Nazwa magazynu wymaga od 3 do 20 znaków")
    @Column(length = 20)
    private String magazine;

    @NotBlank(message = "Imię i nazwisko autora są wymagane")
    @Size(min = 3, max = 30, message = "Nazwa magazynu wymaga od 3 do 20 znaków")
    @Column(length = 30)
    private String author;

    @NotBlank(message = "Data publikacji jest wymagana")
    private LocalDateTime published;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
