package com.mz.press.controllers;


import com.mz.press.models.Article;
import com.mz.press.services.ArticleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
public class ArticleRestController {

    private final ArticleService service;

    @GetMapping("rest/articles")
    public List<Article> getArticles(@RequestParam(required = false) String title,
                                     @RequestParam(required = false) String content) {
        return service.getArticleByParams(title, content);
    }

    @GetMapping("rest/articles/{id}")
    public ResponseEntity<Article> getArticle(@PathVariable Long id) {
        var optionalArticle = service.findArticle(id);
        if (optionalArticle.isPresent()) {
            return ResponseEntity.ok(optionalArticle.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("rest/articles")
    public ResponseEntity<Article> addArticle(@RequestBody @Valid Article article) {
        return ResponseEntity.ok(service.addArticle(article));
    }

    @PutMapping("rest/articles/{id}")
    public ResponseEntity<Article> editArticle(@PathVariable Long id, @RequestBody @Valid Article article) {
        return ResponseEntity.ok(service.updateArticle(id, article));
    }

    @DeleteMapping("rest/articles/{id}")
    public ResponseEntity<Void> removeArticle(@PathVariable Long id) {
        var optionalArticle = service.findArticle(id);
        if (optionalArticle.isPresent()) {
            service.removeArticle(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
